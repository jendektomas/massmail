# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.coffe.
# You can use CoffeeScript in this file: http://coffeescript.org/
# This is a manifest file that'll be compiled into application.js, which will include all the files
# listed below.

#= require jquery
#= require jquery_ujs
#= require turbolinks
#= require bootstrap-sprockets
#= require bootstrap-wysihtml5
#= require ckeditor/init

$ = jQuery

$ ->
  $(document).on 'page:load', ->

    $('.wysihtml5').each (i, elem) ->
      $(elem).wysihtml5({'toolbar': {
        'font-styles': true,
        'color': false,
        'emphasis': {
          'small': true
        },
        'blockquote': true,
        'lists': true,
        'html': false,
        'link': true,
        'image': true,
        'smallmodals': false
      }})
