require 'csv'

class CampaignsController < ApplicationController

  def index
    @campaigns = Campaign.all.order(created_at: :desc)
  end

  def show
    @campaign = Campaign.find(params[:id])
    @groups = @campaign.groups.pluck(:id)

    @recipients = Recipient.where(:group_id => @groups).page params[:page]

  end

  def new
    @campaign = Campaign.new
    @groups = Group.all
  end

  def create
    @campaign = Campaign.new(campaign_params)
    @campaign.save

    group_ids = params[:campaign][:group_ids]
    group = Group.where(:id => group_ids)
    @campaign.groups = group

    redirect_to @campaign
  end

  def edit
    @campaign = Campaign.find(params[:id])
    @groups = Group.where(:blacklist => false)
  end

  def update
    @campaign = Campaign.find(params[:id])
    @campaign.update(campaign_params)

    group_ids = params[:campaign][:group_ids]
    group = Group.where(:id => group_ids)
    @campaign.groups = group

    redirect_to @campaign
  end

  def destroy
    Campaign.find(params[:id]).destroy
    redirect_to @campaign
  end

  def send_bulk_email
    @campaign = Campaign.find(params[:campaign_id])

    @groups = @campaign.groups

    senders = []
    senders << @campaign.sender

    senders << @campaign.sender2 unless @campaign.sender2.empty?
    senders << @campaign.sender3 unless @campaign.sender3.empty?


    @blacklist_emails = Group.where(:blacklist => true).first.recipients

    @blacklist_emails =  @blacklist_emails.pluck(:email)


    @recipients = []
    @groups.each do |group|
      @recipients << group.recipients
    end

    @recipients.each do |rec_group|
      rec_group.each do |recipient|

        next if @blacklist_emails.include? recipient.email

        if SentMail.where(:campaign_id => @campaign.id, :email => recipient.email).count > 0
          puts recipient.email
          puts "already sent"
          puts "----"
          already_sent = true
        else
          already_sent = false
          puts recipient.email
          puts "not send.. sending"
          puts "----"
        end

        unless already_sent
          SentMail.create(:campaign_id => @campaign.id, :email => recipient.email)

          @body = @campaign.text.dup
          @footer = @campaign.footer.dup

          @text = @body.sub! '[::name]', recipient.name
          @text = @body.sub! '[::surname]', recipient.surname
          @text = @body if @text.nil?

          sender = senders.sample

          sleep(rand(3..10))
          Pony.mail({
                        :from => sender, :via => :smtp,
                        :headers => { 'Content-Type' => 'text/html' },
                        :via_options => { :address => 'smtp.websupport.sk', :port => 25, :domain => "websupport.sk",
                                          :user_name => sender,
                                          :password => "Randompwd2",
                                          :authentication => "plain",
                                          :domain => "websupport.sk"},
                        :to => recipient.email,
                        :subject => @campaign.subject,
                        :body => @text+@footer
                    })
        end
      end
    end

    redirect_to campaign_path(@campaign)
  end


  private

  def campaign_params
    params.require(:campaign).permit(:name, :subject, :text, :footer, :sender, :sender2, :sender3, :file, :group_ids)
  end
end
