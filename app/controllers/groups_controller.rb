class GroupsController < ApplicationController
  require 'csv'

  def index
    @groups = Group.all
  end

  def show
    @group = Group.find(params[:id])
    @recipients = @group.recipients.page params[:page]
  end

  def new
    @group = Group.new
  end

  def create
    @group = Group.new(group_params)
    @group.save

    redirect_to @group
  end


  def edit
    @group = Group.find(params[:id])
  end

  def update
    @group = Group.find(params[:id])
    @group.update(group_params)

    redirect_to @group
  end


  def import_recipients
    @group = Group.find(params[:group_id])

    csv_text = File.read(params[:file].path)
    csv = CSV.parse(csv_text, :headers => false)
    csv.each do |row|
      recipient = Recipient.new(:email => row[0], :name => row[1], :surname => row[2], :source => row[3])
      recipient.group_id = params[:group_id]
      recipient.save
    end

    redirect_to group_path(@group)
  end



  private

  def group_params
    params.require(:group).permit(:name, :description, :blacklist)
  end
end
