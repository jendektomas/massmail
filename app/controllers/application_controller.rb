require "application_responder"

class ApplicationController < ActionController::Base
  self.responder = ApplicationResponder
  respond_to :html

  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  http_basic_authenticate_with name: "tomas", password: "tomas"

  #include ActionController::MimeResponds

  #include ActionController::MimeResponds
  #include ActionController::ImplicitRender

  include ActionController::RespondWith
end
