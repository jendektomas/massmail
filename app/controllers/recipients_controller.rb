class RecipientsController < ApplicationController

  def destroy
    @recipient = Recipient.find(params[:id])
    group = @recipient.group
    @recipient.destroy

    redirect_to group
  end
end
