class Recipient < ActiveRecord::Base
  require 'valid_email'
  include ActiveModel::Validations

  belongs_to :group

  validates_uniqueness_of :email, :scope => :group_id

  validates :email, :presence => true, :email => true

  paginates_per 100
end
