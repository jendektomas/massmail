class Group < ActiveRecord::Base
  has_many :recipients
  has_and_belongs_to_many :campaign

  paginates_per 20
end
