class CreateCampaignsGroups < ActiveRecord::Migration

  def change
    create_table :campaigns_groups do |t|
      t.belongs_to :campaign, index:true
      t.belongs_to :group, index:true
    end
  end

end


