class AddFieldsToCampaign < ActiveRecord::Migration
  def change
    add_column :campaigns, :subject, :text
    add_column :campaigns, :text, :text
    add_column :campaigns, :footer, :text
    add_column :campaigns, :sender, :text
  end
end
