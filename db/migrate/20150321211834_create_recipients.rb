class CreateRecipients < ActiveRecord::Migration
  def change
    create_table :recipients do |t|
      t.text :email
      t.string :name
      t.string :surname
      t.text :source
      t.string :active
      t.timestamps null: false
    end
  end
end
