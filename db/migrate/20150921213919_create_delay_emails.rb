class CreateDelayEmails < ActiveRecord::Migration
  def change
    create_table :delay_emails do |t|

      t.timestamps null: false
    end
  end
end
