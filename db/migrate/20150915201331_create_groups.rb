class CreateGroups < ActiveRecord::Migration
  def change
    create_table :groups do |t|
      t.belongs_to :campaign
      t.text :name
      t.text :description
      t.timestamps null: false
    end
  end
end
