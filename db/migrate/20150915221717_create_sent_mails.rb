class CreateSentMails < ActiveRecord::Migration
  def change
    create_table :sent_mails do |t|
      t.text :email
      t.integer :campaign_id
      t.integer :group_id
      t.timestamps null: false
    end
  end
end
