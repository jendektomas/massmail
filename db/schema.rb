# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150921213919) do

  create_table "campaigns", force: :cascade do |t|
    t.text     "name",       limit: 65535
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
    t.text     "subject",    limit: 65535
    t.text     "text",       limit: 65535
    t.text     "footer",     limit: 65535
    t.text     "sender",     limit: 65535
    t.text     "sender2",    limit: 65535
    t.text     "sender3",    limit: 65535
  end

  create_table "campaigns_groups", force: :cascade do |t|
    t.integer "campaign_id", limit: 4
    t.integer "group_id",    limit: 4
  end

  add_index "campaigns_groups", ["campaign_id"], name: "index_campaigns_groups_on_campaign_id", using: :btree
  add_index "campaigns_groups", ["group_id"], name: "index_campaigns_groups_on_group_id", using: :btree

  create_table "ckeditor_assets", force: :cascade do |t|
    t.string   "data_file_name",    limit: 255, null: false
    t.string   "data_content_type", limit: 255
    t.integer  "data_file_size",    limit: 4
    t.integer  "assetable_id",      limit: 4
    t.string   "assetable_type",    limit: 30
    t.string   "type",              limit: 30
    t.integer  "width",             limit: 4
    t.integer  "height",            limit: 4
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "ckeditor_assets", ["assetable_type", "assetable_id"], name: "idx_ckeditor_assetable", using: :btree
  add_index "ckeditor_assets", ["assetable_type", "type", "assetable_id"], name: "idx_ckeditor_assetable_type", using: :btree

  create_table "delay_emails", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "groups", force: :cascade do |t|
    t.integer  "campaign_id", limit: 4
    t.text     "name",        limit: 65535
    t.text     "description", limit: 65535
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
    t.boolean  "blacklist",   limit: 1
  end

  create_table "recipients", force: :cascade do |t|
    t.text     "email",       limit: 65535
    t.string   "name",        limit: 255
    t.string   "surname",     limit: 255
    t.text     "source",      limit: 65535
    t.string   "active",      limit: 255
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
    t.integer  "campaign_id", limit: 4
    t.integer  "group_id",    limit: 4
  end

  create_table "sent_mails", force: :cascade do |t|
    t.text     "email",       limit: 65535
    t.integer  "campaign_id", limit: 4
    t.integer  "group_id",    limit: 4
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
  end

end
